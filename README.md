1. Turn off Word Wrap in Notepad++.
2. Open the repo version of 'org.eclipse.ui.workbench.prefs'.
    a. Find the <org.eclipse.ui.commands> tag (should be on line 34).
    b. Save that tag in '1.xml' in the repo.
    c. Remove all of the literal "\r\n" strings.
    d. Replace all of the literal "\=" strings with just "=".
    e. NOW format the document using the XML Plugin's "Pretty Print" command (Alt+x).
3. Repeat all those steps for the latest version of the prefs file, using '2.xml' in the repo.
